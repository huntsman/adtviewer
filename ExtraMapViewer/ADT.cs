﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using ExtraMapViewer.Chunks;

namespace ExtraMapViewer
{
    class ADT
    {
        BinaryReader Reader;
        public List<Chunk> chunks;

        public ADT(BinaryReader br)
        {
            Reader = br;
            chunks = new List<Chunk>();
        }

        public void init()
        {
            while (Reader.BaseStream.Position != Reader.BaseStream.Length)
            {
                byte[] chunk = Reader.ReadBytes(4);
                Int32 size = Reader.ReadInt32();

                IEnumerable<byte> reversedChunk = chunk.Reverse();
                string a = Encoding.ASCII.GetString(reversedChunk.ToArray(), 0, 4);

                switch (a)
                {
                    case "MVER":
                    {
                        MVER mver = new MVER(Reader.ReadBytes(size));
                        mver.Read();
                        chunks.Add(mver);
                        break;
                    }
                    case "MHDR":
                    {
                        MHDR mhdr = new MHDR(Reader.ReadBytes(size));
                        mhdr.Read();
                        chunks.Add(mhdr);
                        break;
                    }
                    case "MCNK":
                    {
                        MCNK mcnk = new MCNK(Reader.ReadBytes(size));
                        mcnk.Read();
                        chunks.Add(mcnk);
                        break;
                    }
                    default:
                    {
                        Chunk c = new Chunk(Reader.ReadBytes(size), a);
                        c.Read();
                        chunks.Add(c);
                        break;
                    }
                }
            }
        }
    }
}
