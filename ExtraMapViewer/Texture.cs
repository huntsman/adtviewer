﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace ExtraMapViewer
{
    class Textures
    {
        DrawManager DrawManager;

        public List<TextureImage> TextureImage;


        public Textures(DrawManager dm)
        {
            TextureImage = new List<TextureImage>();
            DrawManager = dm;
        }
        uint index;
        public void LoadTexture(string FileName)
        {

            Bitmap bm = (Bitmap)Image.FromFile(FileName);

            TextureImage ti = new TextureImage();

            // Получаем данные текстуры
            ti.width = bm.Width;          // Ширина
            ti.height = bm.Height;        // Высота
            ti.bpp = 3;      // Байт на пиксель(будем считать, что RGB)

            ti.imageData = GetRGB(bm);

            ti.texID = new uint[1];
            ti.texID[0] = index;
            index++;

            TextureImage.Add(ti);

            DrawManager.BindingTexture(ti);

        }

        public byte[] GetRGB(Bitmap bmp)
        {
            bmp.RotateFlip(RotateFlipType.Rotate180FlipX);
            BitmapData bmp_data = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            IntPtr ptr = bmp_data.Scan0;
            int num_bytes = bmp_data.Stride * bmp.Height;
            byte[] rgb = new byte[num_bytes];
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgb, 0, num_bytes);

            return rgb;
        }

    }

    public struct TextureImage
    {
        public byte[] imageData;     // Данные текстуры
        public int bpp;            // Байт на пиксел
        public int width, height;        // Высота и ширина
        public uint[] texID;           // ID текстуры
    }
}
