﻿namespace ExtraMapViewer
{
    partial class DrawManager
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // DrawManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "DrawManager";
            this.Size = new System.Drawing.Size(404, 272);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DrawManager_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.DrawManager_KeyUp);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.DrawManager_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.DrawManager_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.DrawManager_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
