﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ExtraMapViewer
{
    class Camera
    {
        //Инициализируем вектор нашей позиции в нулевые координаты
        public Vector3 Position;
        public Vector3 View;     // Иниц. вектор взгляда
        public Vector3 UpVector;       // Вертикальный вектор
        public Vector3 Strafe;
        public float Speed;

        //int SCREEN_WIDTH;
        //int SCREEN_HEIGHT;
        //DrawManager DrawManager;

        public Camera(/*DrawManager dm*/)
        {
            Position = new Vector3(0.0f, 0.0f, 8.0f);
            View = new Vector3(0.0f, 0.0f, 0.0f);
            UpVector = new Vector3(0.0f, 1.0f, 0.0f);
            Speed = 0.60f; //0.03f
            //SCREEN_WIDTH = dm.Width;
            //SCREEN_HEIGHT = dm.Height;
            //DrawManager = dm;

        }
        //m_vPosition = vZero;
        //m_vView = vView;
        //m_vUpVector = vUp;

        ///////////////////////////////// POSITION CAMERA \\\\\\\\\\\\\\\\*
        /////
        /////   Устанавливает позицию, взгляд, верт. вектор камеры
        /////
        ///////////////////////////////// POSITION CAMERA \\\\\\\\\\\\\\\\*

        public void PositionCamera(float positionX, float positionY, float positionZ,
                float viewX, float viewY, float viewZ,
                float upVectorX, float upVectorY, float upVectorZ)
        {
            Vector3 vPosition = new Vector3(positionX, positionY, positionZ);
            Vector3 vView = new Vector3(viewX, viewY, viewZ);
            Vector3 vUpVector = new Vector3(upVectorX, upVectorY, upVectorZ);

            //Обширный код просто делает легче назначение переменных.
            //Можно просто сразу присвоить переменным класса переданные функции значения.

            Position = vPosition;
            View = vView;
            UpVector = vUpVector;
        }

        ///////////////////////////////// STRAFE CAMERA \\\\\\\\\\\\\\\\*
/////
/////              стрейфит камеру в стороны:
/////
///////////////////////////////// STRAFE CAMERA \\\\\\\\\\\\\\\\*
        public void StrafeCamera(bool Inverse)
        {

            float spd = Speed;
            if (Inverse)
                spd *= (-1);

            // добавим вектор стрейфа к позиции
            Position.x += Strafe.x * spd;
            Position.y += Strafe.y * spd;
            Position.z += Strafe.z * spd;

            // Добавим теперь к взгляду
            View.x += Strafe.x * spd;
            View.y += Strafe.y * spd;
            View.z += Strafe.z * spd;
        }


        ///////////////////////////////// MOVE CAMERA \\\\\\\\\\\\\\\\*
        /////
        /////   Движение камеры вперед-назад с заданной скоростью
        /////
        ///////////////////////////////// MOVE CAMERA \\\\\\\\\\\\\\\\*

        public void MoveCamera(bool Inverse)
        {
            Vector3 vVector = View - Position;

            //vVector.y = 0.0f;
            vVector = Normalize(vVector);

            float spd = Speed;
            if (Inverse)
                spd *= (-1);

            Position.x += vVector.x * spd;
            Position.y += vVector.y * spd;
            Position.z += vVector.z * spd;
            View.x += vVector.x * spd;
            View.y += vVector.y * spd;
            View.z += vVector.z * spd;

        }

        public Vector3 Cross(Vector3 vVector1, Vector3 vVector2)
        {
            Vector3 vNormal = new Vector3();

            // Если у нас есть 2 вектора (вектор взгляда и вертикальный вектор), 
            // у нас есть плоскость, от которой мы можем вычислить угол в 90 градусов.
            // Рассчет cross'a прост, но его сложно запомнить с первого раза. 
            // Значение X для вектора - (V1.y * V2.z) - (V1.z * V2.y)
            vNormal.x = ((vVector1.y * vVector2.z) - (vVector1.z * vVector2.y));

            // Значение Y - (V1.z * V2.x) - (V1.x * V2.z)
            vNormal.y = ((vVector1.z * vVector2.x) - (vVector1.x * vVector2.z));

            // Значение Z: (V1.x * V2.y) - (V1.y * V2.x)
            vNormal.z = ((vVector1.x * vVector2.y) - (vVector1.y * vVector2.x));

            // *ВАЖНО* Вы не можете менять этот порядок, иначе ничего не будет работать.
            // Должно быть именно так, как здесь. Просто запомните, если вы ищите Х, вы не
            // используете значение X двух векторов, и то же самое для Y и Z. Заметьте,
            // вы рассчитываете значение из двух других осей, и никогда из той же самой.

            // Итак, зачем всё это? Нам нужно найти ось, вокруг которой вращаться. Вращение камеры
            // влево и вправо простое - вертикальная ось всегда (0,1,0). 
            // Вращение камеры вверх и вниз отличается, так как оно происходит вне 
            // глобальных осей. Достаньте себе книгу по линейной алгебре, если у вас 
            // её ещё нет, она вам пригодится.

            // вернем результат.
            return vNormal;
        }

        float Magnitude(Vector3 vNormal)
        {
            // Это даст нам величину нашей нормали, 
            // т.е. длину вектора. Мы используем эту информацию для нормализации
            // вектора. Вот формула: magnitude = sqrt(V.x^2 + V.y^2 + V.z^2)   где V - вектор.

            return (float)Math.Sqrt((vNormal.x * vNormal.x) +
                    (vNormal.y * vNormal.y) +
                    (vNormal.z * vNormal.z));
        }

        public Vector3 Normalize(Vector3 vVector)
        {
            // Вы спросите, для чего эта ф-я? Мы должны убедиться, что наш вектор нормализирован.
            // Вектор нормализирован - значит, его длинна равна 1. Например,
            // вектор (2,0,0) после нормализации будет (1,0,0).

            // Вычислим величину нормали
            float magnitude = Magnitude(vVector);

            // Теперь у нас есть величина, и мы можем разделить наш вектор на его величину.
            // Это сделает длинну вектора равной единице, так с ним будет легче работать.
            vVector = vVector / magnitude;

            return vVector;
        }

        public void Rotate_View(float speed)
        {
            Vector3 vVector = new Vector3();// Полчим вектор взгляда
            vVector.x = View.x - Position.x;
            vVector.y = View.y - Position.y;
            vVector.z = View.z - Position.z;



            View.z = (float)(Position.z + Math.Sin(speed) * vVector.x + Math.Cos(speed) * vVector.z);
            View.x = (float)(Position.x + Math.Cos(speed) * vVector.x - Math.Sin(speed) * vVector.z);
        }

        public void Rotate_Y(float speed)
        {
            Vector3 vVector = new Vector3();// Полчим вектор взгляда
            vVector.x = View.x - Position.x;
            vVector.y = View.y - Position.y;
            vVector.z = View.z - Position.z;





            View.z = (float)(Position.z + Math.Sin(speed) * vVector.y + Math.Cos(speed) * vVector.z);
            //View.y = (float)(Position.y + Math.Cos(speed) * vVector.y - Math.Sin(speed) * vVector.z);
            //View.x = (float)(Position.x + Math.Cos(speed) * vVector.x - Math.Sin(speed) * vVector.z);
            View.y = (float)(Position.y + Math.Cos(speed) * vVector.y - Math.Sin(speed) * vVector.z);

            //Position.z = (float)(Position.z * Math.Cos(speed) - Position.y * Math.Sin(speed));
            //Position.y = (float)(Position.y * Math.Cos(speed) + Position.z * Math.Sin(speed));

        }

        public void RotateView(float angle, float x, float y, float z)
        {
            Vector3 vNewView = new Vector3();
            Vector3 vView = new Vector3();

            // Получим наш вектор взгляда (направление, куда мы смотрим)
            vView.x = View.x - Position.x;    //направление по X
            vView.y = View.y - Position.y;    //направление по Y
            vView.z = View.z - Position.z;    //направление по Z

            // Теперь, имея вектор взгляда, хранящийся в CVector3 "vView", мы можем вращать его.
            // Эта ф-я будет вызыватся, когда нужно повернутся налево-направо.
            // Итак, нам нужно вращаться вокруг нашей позиции. Представьте это примерно так:
            // скажем, мы стоим на месте, смотря вперед. Мы смотрим в какую-то точку, верно?
            // Теперь, если мы повернем голову налево или направо, направление взгляда изменится.
            // Соответственно переместится точка, на которую мы смотрим (вектор взгляда).
            // Вот почему мы изменяем m_vView - потому что это и есть вектор 
            // взгляда. Мы будем вращать m_vView вокруг m_vPosition
            // по кругу, чтобы реализовать всё это.
            // Чтобы вращать что-то, используем синус и косинус. Для использования sin() & cos() мы
            // ранее подключили math.h.
            //
            // Чтобы реализовать вращение камеры, мы будем использовать axis-angle вращение.
            // Я должен предупредить, что формулы для рассчета вращения не очень просты, но
            // занимают не много кода. Axis-angle вращение позволяет нам вращать точку в пространстве
            // вокруг нужной оси. Это значит, что мы можем взять нашу точку взгляда (m_vView) и
            // вращать вокруг нашей позиции.
            // Чтобы лучше понять следующие рассчеты, советую вам посмотреть детальное
            // описание: http://astronomy.swin.edu.au/~pbourke/geometry/rotate/

            // Рассчитаем 1 раз синус и косинус переданного угла
            float cosTheta = (float)Math.Cos(angle);
            float sinTheta = (float)Math.Sin(angle);

            // Найдем новую позицию X для вращаемой точки
            vNewView.x = (cosTheta + (1 - cosTheta) * x * x) * vView.x;
            vNewView.x += ((1 - cosTheta) * x * y - z * sinTheta) * vView.y;
            vNewView.x += ((1 - cosTheta) * x * z + y * sinTheta) * vView.z;

            // Найдем позицию Y
            vNewView.y = ((1 - cosTheta) * x * y + z * sinTheta) * vView.x;
            vNewView.y += (cosTheta + (1 - cosTheta) * y * y) * vView.y;
            vNewView.y += ((1 - cosTheta) * y * z - x * sinTheta) * vView.z;

            // И позицию Z
            vNewView.z = ((1 - cosTheta) * x * z - y * sinTheta) * vView.x;
            vNewView.z += ((1 - cosTheta) * y * z + x * sinTheta) * vView.y;
            vNewView.z += (cosTheta + (1 - cosTheta) * z * z) * vView.z;

            // Теперь просто добавим новый вектор вращения к нашей позиции, чтобы
            // установить новый взгляд камеры.
            View.x = Position.x + vNewView.x;
            View.y = Position.y + vNewView.y;
            View.z = Position.z + vNewView.z;
        }


        public void SetViewByMouse(Point MousePos, Point ClickMousePos)
        {
            if (ClickMousePos.IsEmpty)
                return;

            // Оператор " >> 1" - то же самое, что деление на 2, но о**енно быстрее ;)
            //int middleX = SCREEN_WIDTH >> 1;   // Вычисляем половину ширины
            //int middleY = SCREEN_HEIGHT >> 1;   // И половину высоты экрана
            float angleY = 0.0f;    // Направление взгляда вверх/вниз  
            float angleZ = 0.0f;    // Значение, необходимое для вращения влево-вправо (по оси Y)
            float currentRotX = 0.0f;


            // Если курсор остался в том же положении, мы не вращаем камеру
            if ((MousePos.X == ClickMousePos.X) && (MousePos.Y == ClickMousePos.Y)) return;

            //// Теперь, получив координаты курсора, возвращаем его обратно в середину.
            //SetCursorPos(middleX, middleY);
            //System.Windows.Forms.Cursor.Position = DrawManager.PointToScreen(new Point(middleX, middleY));
            

            // Теперь нам нужно направление (или вектор), куда сдвинулся курсор.
            // Его рассчет - простое вычитание. Просто возьмите среднюю точку и вычтите из неё
            // новую позицию мыши: VECTOR = P1 - P2; где P1 - средняя точка (400,300 при 800х600).
            // После получения дельты X и Y (или направления), я делю значение 
            // на 1000, иначе камера будет жутко быстрой.
            angleY = (float)((ClickMousePos.X - MousePos.X)) / 1000.0f;
            angleZ = (float)((ClickMousePos.Y - MousePos.Y)) / 1000.0f;


            //Чувствительность мыши
            angleY *= 20;
            angleZ *= 20;

            //OldMPos = MousePos;

            float lastRotX = 0.0f;
            lastRotX = currentRotX;     // Сохраняем последний угол вращения 
            // и используем заново currentRotX

            // Если текущее вращение больше 1 градуса, обрежем его, чтобы не вращать слишком быстро
            if (currentRotX > 1.0f)
            {
                currentRotX = 1.0f;

                // врощаем на оставшийся угол
                if (lastRotX != 1.0f)
                {
                    // Чтобы найти ось, вокруг которой вращаться вверх и вниз, нужно 
                    // найти вектор, перпендикулярный вектору взгляда камеры и 
                    // вертикальному вектору.
                    // Это и будет наша ось. И прежде чем использовать эту ось, 
                    // неплохо бы нормализовать её.
                    Vector3 vAxis = Cross(View - Position, UpVector);
                    vAxis = Normalize(vAxis);

                    // Вращаем камеру вокруг нашей оси на заданный угол
                    RotateView(1.0f - lastRotX, vAxis.x, vAxis.y, vAxis.z);
                    //Rotate_View(1.0f - lastRotX);
                }
            }

            // Если угол меньше -1.0f, убедимся, что вращение не продолжится
            else if (currentRotX < -1.0f)
            {
                currentRotX = -1.0f;
                if (lastRotX != -1.0f)
                {
                    // Опять же вычисляем ось
                    Vector3 vAxis = Cross(View - Position, UpVector);
                    vAxis = Normalize(vAxis);

                    // Вращаем
                    RotateView(-1.0f - lastRotX, vAxis.x, vAxis.y, vAxis.z);
                    //Rotate_View(1.0f - lastRotX);
                }
            }
            // Если укладываемся в пределы 1.0f -1.0f - просто вращаем
            else
            {
                Vector3 vAxis = Cross(View - Position, UpVector);
                vAxis = Normalize(vAxis);
                RotateView(angleZ, vAxis.x, vAxis.y, vAxis.z);
                //RotateView(angleZ, 1, 0, 0);
                //Rotate_View(angleZ);
                //Rotate_Y(-angleZ);
            }

            // Всегда вращаем камеру вокруг Y-оси
            RotateView(angleY, 0, 1, 0);
            //Rotate_View(-angleY);
        }
    }
}