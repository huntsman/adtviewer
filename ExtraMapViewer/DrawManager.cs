﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExtraMapViewer.Chunks;

namespace ExtraMapViewer
{
    public partial class DrawManager : OpenGLControl
    {

        //Camera Camera;
        public bool StartDrawing;
        Textures Textures;
        public Vector3 CameraRotation;
        public Vector3 CameraPosition;
        public float MoveSpeed;


        public DrawManager()
        {
            InitializeComponent();
            PresedKeys = new List<Keys>();
            Textures = new Textures(this);
            CameraRotation = new Vector3();
            CameraPosition = new Vector3();
            MoveSpeed = 1.0f;

        }

        public void OpenGLInitialization()
        {
            // Активируем контекст OpenGL
            ActivateContext();

            Textures.LoadTexture("./2.png");
            Textures.LoadTexture("./b.png");
        }


        public void Draw()
        {
            glBindTexture(GL_TEXTURE_2D, Textures.TextureImage[0].texID[0]);
            glBegin(GL_TRIANGLES);
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 4; y++)
                {


                    glTexCoord2f(0, 0); glVertex3f(y, x, -10);
                    glTexCoord2f(1, 0); glVertex3f(y + 1, x, -10);
                    glTexCoord2f(0.5f, 0.5f); glVertex3f(y + 0.5f, x + 0.5f, -10);


                    glTexCoord2f(0, 0); glVertex3f(y, x, -10);
                    glTexCoord2f(0, 1); glVertex3f(y, x + 1, -10);
                    glTexCoord2f(0.5f, 0.5f); glVertex3f(y + 0.5f, x + 0.5f, -10);


                    glTexCoord2f(0, 1); glVertex3f(y, x + 1, -10);
                    glTexCoord2f(1, 1); glVertex3f(y + 1, x + 1, -10);
                    glTexCoord2f(0.5f, 0.5f); glVertex3f(y + 0.5f, x + 0.5f, -10);

                    glTexCoord2f(1, 0); glVertex3f(y + 1, x, -10);
                    glTexCoord2f(1, 1); glVertex3f(y + 1, x + 1, -10);
                    glTexCoord2f(0.5f, 0.5f); glVertex3f(y + 0.5f, x + 0.5f, -10);

                }
            }
            glEnd();
            //for (float i = 0; i < 100; i += 5)
            //{
            //    glColor3ub(255, 255, 0);
            //    glVertex3f(-10 + i / 5, i, 10 - i / 5);
            //    glColor3ub(255, 0, 0);
            //    glVertex3f(-10 + i / 5, i, -10 + i / 5);
            //    glColor3ub(0, 255, 255);
            //    glVertex3f(10 - i / 5, i, -10 + i / 5);
            //    glColor3ub(0, 0, 255);
            //    glVertex3f(10 - i / 5, i, 10 - i / 5);
            //}

        }

        public void BindingTexture(TextureImage ti)
        {
            glGenTextures(1, ti.texID);
            glBindTexture(GL_TEXTURE_2D, ti.texID[0]);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexImage2D(GL_TEXTURE_2D, 0, 3, ti.height, ti.width, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, ti.imageData);
        }

        public void BeginDraw()
        {
            glClearColor((float)0, (float)0, (float)0, (float)BackColor.A / 0);
            //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            //glMatrixMode(GL_PROJECTION);
            //glLoadIdentity();
            //gluPerspective(130, 1, 50, 0);
            //gluLookAt(0,0,6,0,0,0,0,1,0);

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            gluPerspective(60.0, this.Width / this.Height, 1.0, 2000.0);
            glViewport(0, 0, this.Width, this.Height);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            //glCullFace(GL_BACK);
            //glEnable(GL_CULL_FACE);

            glRotatef(CameraRotation.y, 1, 0, 0);
            glRotatef(CameraRotation.x, 0, 0, 1);

            glTranslatef(-CameraPosition.x, -CameraPosition.y, -CameraPosition.z);


            // Включаем текстуры
            glEnable(GL_TEXTURE_2D);

            glMatrixMode(GL_TEXTURE);
            glPushMatrix();
            glLoadIdentity();
            //glBindTexture(GL_TEXTURE_2D, textures[0].texID);

        }

        public void EndDraw()
        {
            // Переключаем буфера
            SwapBuffers();
            //// Деактивируем контекст OpenGL
            //DeactivateContext();
        }

        public void DrawMap(MCNK adt_MCNK)
        {
            foreach (Chunk subchunk in adt_MCNK.subchunksList)
            {
                if (subchunk.chunkname == "MCVT")
                {
                    MCVT adt_MCVT = (MCVT)subchunk;

                    // Рисуем в плоскости OXY
                    glBindTexture(GL_TEXTURE_2D, Textures.TextureImage[1].texID[0]);
                    glBegin(GL_LINE_STRIP);
                    //glBegin(GL_TRIANGLES);
                    for (int x = 0; x < 8; x++)
                    {
                        for (int y = 0; y < 8; y++)
                        {
                            float nL1 = adt_MCVT.getValNoLOD(x, y);
                            float nL2 = adt_MCVT.getValNoLOD(x, y + 1);
                            float nL3 = adt_MCVT.getValNoLOD(x + 1, y);
                            float nL4 = adt_MCVT.getValNoLOD(x + 1, y + 1);
                            float L = adt_MCVT.getValLOD(x, y);

                            glTexCoord2f(0, 0); glVertex3f(adt_MCNK.IndexY * 8 + y, adt_MCNK.IndexX * 8 + x, nL1 + adt_MCNK.position.y);
                            glTexCoord2f(1, 0); glVertex3f(adt_MCNK.IndexY * 8 + y + 1, adt_MCNK.IndexX * 8 + x, nL2 + adt_MCNK.position.y);
                            glTexCoord2f(0.5f, 0.5f); glVertex3f(adt_MCNK.IndexY * 8 + y + 0.5f, adt_MCNK.IndexX * 8 + x + 0.5f, L + adt_MCNK.position.y);

                            glTexCoord2f(0, 0); glVertex3f(adt_MCNK.IndexY * 8 + y, adt_MCNK.IndexX * 8 + x, nL1 + adt_MCNK.position.y);
                            glTexCoord2f(0, 1); glVertex3f(adt_MCNK.IndexY * 8 + y, adt_MCNK.IndexX * 8 + x + 1, nL3 + adt_MCNK.position.y);
                            glTexCoord2f(0.5f, 0.5f); glVertex3f(adt_MCNK.IndexY * 8 + y + 0.5f, adt_MCNK.IndexX * 8 + x + 0.5f, L + adt_MCNK.position.y);

                            glTexCoord2f(0, 1); glVertex3f(adt_MCNK.IndexY * 8 + y, adt_MCNK.IndexX * 8 + x + 1, nL3 + adt_MCNK.position.y);
                            glTexCoord2f(1, 1); glVertex3f(adt_MCNK.IndexY * 8 + y + 1, adt_MCNK.IndexX * 8 + x + 1, nL4 + adt_MCNK.position.y);
                            glTexCoord2f(0.5f, 0.5f); glVertex3f(adt_MCNK.IndexY * 8 + y + 0.5f, adt_MCNK.IndexX * 8 + x + 0.5f, L + adt_MCNK.position.y);

                            glTexCoord2f(1, 0); glVertex3f(adt_MCNK.IndexY * 8 + y + 1, adt_MCNK.IndexX * 8 + x, nL2 + adt_MCNK.position.y);
                            glTexCoord2f(1, 1); glVertex3f(adt_MCNK.IndexY * 8 + y + 1, adt_MCNK.IndexX * 8 + x + 1, nL4 + adt_MCNK.position.y);
                            glTexCoord2f(0.5f, 0.5f); glVertex3f(adt_MCNK.IndexY * 8 + y + 0.5f, adt_MCNK.IndexX * 8 + x + 0.5f, L + adt_MCNK.position.y);

                        }
                    }
                    glEnd();
                }
            }
        }

        public void Painting()
        {

             //Рисуем квадрат в плоскости OXY
            glBegin(GL_QUADS);
            glVertex3f(0, 0, 0);
            glVertex3f(1, 0, 0);
            glVertex3f(1, 1, 0);
            glVertex3f(0, 1, 0);
            glEnd();

            //glBegin(GL_QUADS);
            //glVertex3f(-2, -2, -2);
            //glVertex3f(-1, -2, -2);
            //glVertex3f(-1, -1, -2);
            //glVertex3f(-2, -1, -2);
            //glEnd();

        }

        Point MouseOld;
        private void DrawManager_MouseMove(object sender, MouseEventArgs e)
        {

            if (MouseKeyPress)
            if (StartDrawing)
            {
                CameraRotation.x = CameraRotation.x + (e.X - MouseOld.X);
                CameraRotation.y = CameraRotation.y + (e.Y - MouseOld.Y);
            }

            MouseOld = e.Location;
        }

        bool MouseKeyPress = false;
        private void DrawManager_MouseDown(object sender, MouseEventArgs e)
        {
                MouseKeyPress = true;
        }

        private void DrawManager_MouseUp(object sender, MouseEventArgs e)
        {
            MouseKeyPress = false;
        }

        private void DrawManager_KeyDown(object sender, KeyEventArgs e)
        {
            //if (StartDrawing)
            //{


            //}
            if (PresedKeys.Count != 0)
            {
                if (PresedKeys[0] != e.KeyCode)
                    PresedKeys.Add(e.KeyCode);
            }
            else
                PresedKeys.Add(e.KeyCode);
            
        }

        List<Keys> PresedKeys;
        private void DrawManager_KeyUp(object sender, KeyEventArgs e)
        {

            for (int i = 0; i < PresedKeys.Count; i++)
            {
                if (PresedKeys[i] == e.KeyCode)
                {

                    PresedKeys.RemoveAt(i);
                    i--;
                }
            }

        }

        bool KeyIsPresed(Keys Key)
        {
            foreach (Keys k in PresedKeys)
            {
                if (k == Key)
                    return true;
            }

            return false;
        }

        public void UpdateKeyInput()
        {
            if (KeyIsPresed(Keys.W))
            {
                CameraPosition.x = (float)(CameraPosition.x - Math.Sin(CameraRotation.x * Math.PI / 180) * MoveSpeed * (-1));
                CameraPosition.y = (float)(CameraPosition.y - Math.Cos(CameraRotation.x * Math.PI / 180) * MoveSpeed * (-1));
                CameraPosition.z = (float)(CameraPosition.z - Math.Sin((CameraRotation.y - 90) * Math.PI / 180) * MoveSpeed * (-1));
            }
            else if (KeyIsPresed(Keys.S))
            {
                CameraPosition.x = (float)(CameraPosition.x - Math.Sin(CameraRotation.x * Math.PI / 180) * MoveSpeed);
                CameraPosition.y = (float)(CameraPosition.y - Math.Cos(CameraRotation.x * Math.PI / 180) * MoveSpeed);
                CameraPosition.z = (float)(CameraPosition.z - Math.Sin((CameraRotation.y - 90) * Math.PI / 180) * MoveSpeed);
            }

            if (KeyIsPresed(Keys.D))
            {
                CameraPosition.x = (float)(CameraPosition.x - Math.Sin((CameraRotation.x - 90) * Math.PI / 180) * MoveSpeed);
                CameraPosition.y = (float)(CameraPosition.y - Math.Cos((CameraRotation.x - 90) * Math.PI / 180) * MoveSpeed);
            }
            else if (KeyIsPresed(Keys.A))
            {
                CameraPosition.x = (float)(CameraPosition.x - Math.Sin((CameraRotation.x - 90) * Math.PI / 180) * MoveSpeed * (-1));
                CameraPosition.y = (float)(CameraPosition.y - Math.Cos((CameraRotation.x - 90) * Math.PI / 180) * MoveSpeed * (-1));
            }
        }
    }
}
