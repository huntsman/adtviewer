﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ExtraMapViewer.Chunks
{
    public class Chunk
    {
        public string chunkname;
        protected BinaryReader Reader;

        public Chunk(byte[] buff, string chunkname)
        {
            this.Reader = new BinaryReader(new MemoryStream(buff));
            this.chunkname = chunkname;
        }
        public virtual void Read()
        {
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0} chunk:", chunkname);
            sb.AppendLine();
            sb.AppendFormat("Readed data: {0} bytes. Total: {1}", this.Reader.BaseStream.Position, this.Reader.BaseStream.Length);
            sb.AppendLine();
            return sb.ToString();
        }
    }
}
