﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraMapViewer.Chunks
{
    enum MHDRFlags
    {
        mhdr_MFBO = 1,                // contains a MFBO chunk.
        mhdr_northrend = 2,           // is set for some northrend ones.
    };

    class MHDR : Chunk
    {
        MHDRFlags flags;
        UInt32 mcin;
        UInt32 mtex;
        UInt32 mmdx;
        UInt32 mmid;
        UInt32 mwmo;
        UInt32 mwid;
        UInt32 mddf;
        UInt32 modf;
        UInt32 mfbo;                     // this is only set if flags & mhdr_MFBO.
        UInt32 mh2o;
        UInt32 mtxf;
        UInt32[] unused;

        public MHDR(byte[] buff) : base(buff, "MHDR") { }
        public override void Read()
        {
            this.flags = (MHDRFlags)base.Reader.ReadUInt32();
            this.mcin = Reader.ReadUInt32();
            this.mtex = Reader.ReadUInt32();
            this.mmdx = Reader.ReadUInt32();
            this.mmid = Reader.ReadUInt32();
            this.mwmo = Reader.ReadUInt32();
            this.mwid = Reader.ReadUInt32();
            this.mddf = Reader.ReadUInt32();
            this.modf = Reader.ReadUInt32();
            this.mfbo = Reader.ReadUInt32();
            this.mh2o = Reader.ReadUInt32();
            this.mtxf = Reader.ReadUInt32();

            this.unused = new UInt32[4];
            for (int i = 0; i < 4; i++)
                unused[i] = Reader.ReadUInt32();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.ToString());
            sb.AppendFormat("Flags: {0}", flags);
            sb.AppendLine();
            sb.AppendFormat("mcin: {0}", mcin);
            sb.AppendLine();
            sb.AppendFormat("mtex: {0}", mtex);
            sb.AppendLine();
            sb.AppendFormat("mmdx: {0}", mmdx);
            sb.AppendLine();
            sb.AppendFormat("mmid: {0}", mmid);
            sb.AppendLine();
            sb.AppendFormat("mddf: {0}", mddf);
            sb.AppendLine();
            sb.AppendFormat("modf: {0}", modf);
            sb.AppendLine();
            sb.AppendFormat("mfbo: {0}", mfbo);
            sb.AppendLine();
            sb.AppendFormat("mh2o: {0}", mh2o).AppendLine();
            sb.AppendFormat("mtxf: {0}", mtxf).AppendLine();

            return sb.ToString();
        }
    };

}
