﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraMapViewer.Chunks
{
    public class MCVT : Chunk
    {
        public const int ADT_CELL_SIZE = 8;
        public const int ADT_CELLS_PER_GRID = 16;
        public const int ADT_GRID_SIZE = (ADT_CELLS_PER_GRID * ADT_CELL_SIZE);

        public float[] height_map;

        public MCVT(byte[] buff)
            : base(buff, "MCVT")
        {
            height_map = new float[(ADT_CELL_SIZE + 1) * (ADT_CELL_SIZE + 1) + ADT_CELL_SIZE * ADT_CELL_SIZE];
        }

        public float getValNoLOD(int x, int y)
        {
            int add = y * 8;

            return height_map[y * 9 + x + add];
        }

        public float getValLOD(int x, int y)
        {
            int add = (y + 1) * 9;

            return height_map[y * 8 + x + add];
        }

        public override void Read()
        {
            for (int i = 0; i < (ADT_CELL_SIZE + 1) * (ADT_CELL_SIZE + 1) + ADT_CELL_SIZE * ADT_CELL_SIZE; i++)
                height_map[i] = base.Reader.ReadSingle();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < (ADT_CELL_SIZE + 1) * (ADT_CELL_SIZE + 1) + ADT_CELL_SIZE * ADT_CELL_SIZE; i++)
                sb.AppendFormat("height_map[{0}]: {1}, ", i, this.height_map[i]);

            return sb.ToString();
        }
    }
}
