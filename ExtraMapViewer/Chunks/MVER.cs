﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraMapViewer.Chunks
{
    class MVER : Chunk
    {
        public UInt32 version;

        public MVER(byte[] buff) : base(buff, "MVER") { }
        public override void Read()
        {
            version = base.Reader.ReadUInt32();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(base.ToString());
            sb.AppendFormat("Version: {0}", version);
            sb.AppendLine();

            return sb.ToString();
        }
    };
}
