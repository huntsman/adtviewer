﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraMapViewer.Chunks
{
    enum MCNKFlags// 03-29-2005 By ObscuR
    {
	    FLAG_MCSH,
	    FLAG_IMPASS,
	    FLAG_LQ_RIVER,
	    FLAG_LQ_OCEAN,
	    FLAG_LQ_MAGMA,
	    FLAG_MCCV,
    }

    public class MCNK : Chunk
    {
        MCNKFlags flags;
        public UInt32 IndexX;
        public UInt32 IndexY;
        UInt32 nLayers;				// maximum 4
        UInt32 nDoodadRefs;
        UInt32 ofsHeight;
        UInt32 ofsNormal;
        UInt32 ofsLayer;
        UInt32 ofsRefs;
        UInt32 ofsAlpha;
        UInt32 sizeAlpha;
        UInt32 ofsShadow;				// only with flags&0x1
        UInt32 sizeShadow;
        UInt32 areaid;
        UInt32 nMapObjRefs;
        UInt32 holes;
        UInt16[] s;	// It is used to determine which detail doodads to show. Values are an array of two bit unsigned integers, naming the layer.
        UInt32 data1;
        UInt32 data2;
        UInt32 data3;
        UInt32 predTex;				// 03-29-2005 By ObscuR; TODO: Investigate
        UInt32 noEffectDoodad;				// 03-29-2005 By ObscuR; TODO: Investigate
        UInt32 ofsSndEmitters;
        UInt32 nSndEmitters;				//will be set to 0 in the client if ofsSndEmitters doesn't point to MCSE!
        UInt32 ofsLiquid;
        UInt32 sizeLiquid;  			// 8 when not used; only read if >8.
        public Vector3 position;
        UInt32 ofsMCCV; 				// only with flags&0x40, had UINT32 textureId; in ObscuR's structure.
        UInt32 ofsMCLV; 				// introduced in Cataclysm
        UInt32 effectId; 			                                // currently unused

        public List<Chunk> subchunksList;

        public MCNK(byte[] buff) : base(buff, "MCNK") 
        {
            this.subchunksList = new List<Chunk>();
        }

        public override void Read()
        {
            this.flags = (MCNKFlags)base.Reader.ReadUInt32();
            this.IndexX = base.Reader.ReadUInt32();
            this.IndexY = base.Reader.ReadUInt32();
            this.nLayers = base.Reader.ReadUInt32();
            this.nDoodadRefs = base.Reader.ReadUInt32();
            this.ofsHeight = base.Reader.ReadUInt32();
            this.ofsNormal = base.Reader.ReadUInt32();
            this.ofsLayer = base.Reader.ReadUInt32();
            this.ofsRefs = base.Reader.ReadUInt32();
            this.ofsAlpha = base.Reader.ReadUInt32();
            this.sizeAlpha = base.Reader.ReadUInt32();
            this.ofsShadow = base.Reader.ReadUInt32();
            this.sizeShadow = base.Reader.ReadUInt32();
            this.areaid = base.Reader.ReadUInt32();
            this.nMapObjRefs = base.Reader.ReadUInt32();
            this.holes = base.Reader.ReadUInt32();

            this.s = new UInt16[2];

            for (int i = 0; i < 2; i++)
                this.s[i] = base.Reader.ReadUInt16();

            this.data1 = base.Reader.ReadUInt32();
            this.data2 = base.Reader.ReadUInt32();
            this.data3 = base.Reader.ReadUInt32();
            this.predTex = base.Reader.ReadUInt32();
            this.noEffectDoodad = base.Reader.ReadUInt32();
            this.ofsSndEmitters = base.Reader.ReadUInt32();
            this.nSndEmitters = base.Reader.ReadUInt32();
            this.ofsLiquid = base.Reader.ReadUInt32();
            this.sizeLiquid = base.Reader.ReadUInt32();

            this.position = new Vector3();
            this.position.z = base.Reader.ReadSingle();
            this.position.x = base.Reader.ReadSingle();
            this.position.y = base.Reader.ReadSingle();

            this.ofsMCCV = base.Reader.ReadUInt32();
            this.ofsMCLV = base.Reader.ReadUInt32();
            this.effectId = base.Reader.ReadUInt32();

            while (base.Reader.BaseStream.Position < base.Reader.BaseStream.Length)
            {
                byte[] subchunk_name = base.Reader.ReadBytes(4);
                IEnumerable<byte> reversedSubChunkName = subchunk_name.Reverse();

                string chunkname = Encoding.ASCII.GetString(reversedSubChunkName.ToArray());
                int size = base.Reader.ReadInt32();

                Chunk subchunk = null;

                switch (chunkname)
                {
                    case "MCVT":
                        subchunk = new MCVT(base.Reader.ReadBytes(size));
                        break;
                    default:
                        subchunk = new Chunk(base.Reader.ReadBytes(size), chunkname);
                        break;
                }

                subchunk.Read();
                subchunksList.Add(subchunk);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("----------------------------------------------");
            sb.Append(base.ToString());

            sb.AppendFormat("Area Id: {0}", areaid).AppendLine();
            sb.AppendFormat("Flags: {0}", this.flags).AppendLine();
            sb.AppendFormat("Index X:Y: {0}:{1}", this.IndexX, this.IndexY).AppendLine();
            sb.AppendFormat("nLayers: {0}", nLayers).AppendLine();
            sb.AppendFormat("nDoodadRefs: {0}", nDoodadRefs).AppendLine();
            sb.AppendFormat("ofsHeight: {0}", ofsHeight).AppendLine();
            sb.AppendFormat("ofsNormal: {0}", ofsNormal).AppendLine();
            sb.AppendFormat("ofsLayer: {0}", ofsLayer).AppendLine();
            sb.AppendFormat("ofsRefs: {0}", ofsRefs).AppendLine();
            sb.AppendFormat("ofsAlpha: {0}", ofsAlpha).AppendLine();
            sb.AppendFormat("sizeAlpha: {0}", sizeAlpha).AppendLine();
            sb.AppendFormat("ofsShadow: {0}", ofsShadow).AppendLine();
            sb.AppendFormat("sizeShadow: {0}", sizeShadow).AppendLine();
            sb.AppendFormat("nMapObjRefs: {0}", nMapObjRefs).AppendLine();
            sb.AppendFormat("Holes: {0}", holes).AppendLine();
            sb.AppendFormat("data1: {0}", data1).AppendLine();
            sb.AppendFormat("data2: {0}", data2).AppendLine();
            sb.AppendFormat("data3: {0}", data3).AppendLine();
            sb.AppendFormat("predTex: {0}", predTex).AppendLine();
            sb.AppendFormat("noEffectDoodad: {0}", noEffectDoodad).AppendLine();
            sb.AppendFormat("ofsSndEmitters: {0}", ofsSndEmitters).AppendLine();
            sb.AppendFormat("ofsLiquid: {0}", ofsLiquid).AppendLine();
            sb.AppendFormat("sizeLiquid: {0}", sizeLiquid).AppendLine();
            sb.AppendFormat("Position: {0}", position).AppendLine();
            sb.AppendFormat("ofsMCCV: {0}", ofsMCCV).AppendLine();
            sb.AppendFormat("ofsMCLV: {0}", ofsMCLV).AppendLine();
            sb.AppendFormat("effectId: {0}", effectId).AppendLine().AppendLine();
            sb.AppendFormat("Sub chunks list:").AppendLine();

            foreach (Chunk subchunk in subchunksList)
            {
                if (subchunk.chunkname == "MCVT")
                {
                    MCVT adt_MCVT = (MCVT)subchunk;

                    sb.AppendLine("Nod converted height:");
                    for (int i = 0; i < 145; i++)
                    {
                        sb.AppendFormat("height_map[{0}]: {1} ", i, adt_MCVT.height_map[i]);
                        if (i % 10 == 0)
                            sb.AppendLine();
                    }

                    sb.AppendLine();

                }
                else
                    sb.Append(subchunk.ToString()).AppendLine();
            }

            sb.AppendLine("----------------------------------------------");

            return sb.ToString();
        }
    }
}
