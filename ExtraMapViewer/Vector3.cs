﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtraMapViewer
{
    public class Vector3
    {
        public float x;
        public float y;
        public float z;

        public Vector3()
        {
            x = 0.0f;
            y = 0.0f;
            z = 0.0f;
        }
        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Vector3 operator +(Vector3 Vector, Vector3 Vector1)
        {
            // Возвращаем +добавленный вектор
            return new Vector3(Vector.x + Vector1.x, Vector.y + Vector1.y, Vector.z + Vector1.z);
        }

        public static Vector3 operator -(Vector3 Vector, Vector3 Vector1)
        {
            return new Vector3(Vector.x - Vector1.x, Vector.y - Vector1.y, Vector.z - Vector1.z);
        }

        public static Vector3 operator *(Vector3 Vector, float num)
        {
            return new Vector3(Vector.x * num, Vector.y * num, Vector.z * num);
        }

        // Перегружаем оператор /
        public static Vector3 operator /(Vector3 Vector, float num)
        {
            return new Vector3(Vector.x / num, Vector.y / num, Vector.z / num);
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("X: {0}, Y: {1}, Z: {2}", x, y, z);
            return sb.ToString();
        }
    }
}
