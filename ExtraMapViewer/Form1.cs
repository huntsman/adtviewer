﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ExtraMapViewer.Chunks;

namespace ExtraMapViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            adt_MCVT = new List<MCVT>();
        }

        float[,] V8;
        float[,] V9;
        ADT adt;

        List<MCVT> adt_MCVT;

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
                adt = new ADT(new BinaryReader(fs));
                adt.init();

                int IndexX = 0;
                int IndexY = 0;

                V8 = new float[(MCVT.ADT_GRID_SIZE + 1), (MCVT.ADT_GRID_SIZE + 1)];
                V9 = new float[(MCVT.ADT_GRID_SIZE + 1), (MCVT.ADT_GRID_SIZE + 1)];

                foreach (Chunk chunk in adt.chunks)
                {
                    if (chunk.chunkname == "MCNK")
                    {
                        MCNK adt_MCNK = (MCNK)chunk;

                        foreach (Chunk subchunk in adt_MCNK.subchunksList)
                        {
                            if (subchunk.chunkname == "MCVT")
                            {
                                adt_MCVT.Add((MCVT)subchunk);

                                /*
                                for (int y = 0; y <= MCVT.ADT_CELL_SIZE; y++)
                                {
                                    int cy = (int)(IndexX * MCVT.ADT_CELL_SIZE + y);
                                    for (int x = 0; x <= MCVT.ADT_CELL_SIZE; x++)
                                    {
                                        int cx = (int)(IndexY * MCVT.ADT_CELL_SIZE + x);
                                        V8[cy, cx] = adt_MCNK.position.y;
                                    }
                                }
                                for (int y = 0; y < MCVT.ADT_CELL_SIZE; y++)
                                {
                                    int cy = (int)(IndexX * MCVT.ADT_CELL_SIZE + y);
                                    for (int x = 0; x < MCVT.ADT_CELL_SIZE; x++)
                                    {
                                        int cx = (int)(IndexY * MCVT.ADT_CELL_SIZE + x);
                                        V9[cy, cx] = adt_MCNK.position.y;
                                    }
                                }
                                 */

                                //// get V9 height map
                                //for (int y = 0; y <= MCVT.ADT_CELL_SIZE; y++)
                                //{
                                //    int cy = (int)(IndexX * MCVT.ADT_CELL_SIZE + y);
                                //    for (int x = 0; x <= MCVT.ADT_CELL_SIZE; x++)
                                //    {
                                //        int cx = (int)(IndexY * MCVT.ADT_CELL_SIZE + x);
                                //        V9[cy, cx] += adt_MCVT.height_map[y * (MCVT.ADT_CELL_SIZE * 2 + 1) + x];
                                //    }
                                //}
                                //// get V8 height map
                                //for (int y = 0; y < MCVT.ADT_CELL_SIZE; y++)
                                //{
                                //    int cy = (int)(IndexX * MCVT.ADT_CELL_SIZE + y);
                                //    for (int x = 0; x < MCVT.ADT_CELL_SIZE; x++)
                                //    {
                                //        int cx = (int)(IndexY * MCVT.ADT_CELL_SIZE + x);
                                //        V8[cy, cx] += adt_MCVT.height_map[y * (MCVT.ADT_CELL_SIZE * 2 + 1) + MCVT.ADT_CELL_SIZE + 1 + x];
                                //    }
                                //}

                            }
                        }

                        //////////////////////////////////////////////
                        IndexX++;

                        if (IndexX >= MCVT.ADT_CELLS_PER_GRID)
                        {
                            IndexX = 0;
                            IndexY++;
                        }
                    }
                    //richTextBox1.AppendText(chunk.ToString());
                    //richTextBox1.AppendText("\r\n\r\n");
                }


                //for (int y = 0; y < MCVT.ADT_GRID_SIZE; y++)
                //{
                //    for (int x = 0; x < MCVT.ADT_GRID_SIZE; x++)
                //    {
                //        richTextBox1.AppendText("V8[" + y +", " + x +"]: " + V8[y, x] + " ");
                //    }
                //    richTextBox1.AppendText("\r\n");
                //}

                //for (int y = 0; y < MCVT.ADT_GRID_SIZE; y++)
                //{
                //    for (int x = 0; x < MCVT.ADT_GRID_SIZE; x++)
                //    {
                //        richTextBox1.AppendText("V9[" + y + ", " + x + "]: " + V9[y, x] + " ");
                //    }
                //    richTextBox1.AppendText("\r\n");
                //}

                fs.Close();
            }
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (DrawManager.StartDrawing == false)
            {
                DrawManager.OpenGLInitialization();
            }

            DrawManager.StartDrawing = true;
            DrawManager.BeginDraw();

            foreach (Chunk chunk in adt.chunks)
            {
                if (chunk.chunkname == "MCNK")
                {
                    DrawManager.DrawMap((MCNK)chunk);
                }
            }

           // foreach (MCVT mcvt in adt_MCVT)
            //{
            //DrawManager.DrawMap(adt_MCVT[3]);
            //}
            DrawManager.Draw();
            //DrawManager.Painting();
            DrawManager.EndDraw();

            DrawManager.UpdateKeyInput();
            
        }

        private void DrawManager_KeyDown(object sender, KeyEventArgs e)
        {

        }

    }
}
